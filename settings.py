"""Settings module for bruteForceCA"""

# Output Directory
outputDir = '/Users/nreiland/Documents/codes/bruteForceCA/output'

# Thalassa Directory
thalassaDir = '/Users/nreiland/Documents/codes/thalassa'

# Inputs (numCores = 'all': run on all available processors)
primariesInput = 'in/primaries.xlsx'
secondariesInput = 'in/secondaries.xlsx'
primariesEqualSecondaries = True
chunkSize = 2
numCores = 'all'
conjunctionThreshold = 1000

### Thalassa Settings for primary and secondary objects

## PHYSICAL MODEL
# insgrav: 0 = sph. grav. field, 1 = non-spherical grav. field.
# isun:   0 = no Sun perturbation, 1 = otherwise.
# imoon:  0 = no Moon perturbation, 1 = otherwise.
# idrag:  0 = no atmospheric drag, 1 = Wertz model, 2 = US76 (PATRIUS), 3 = J77 (Carrara - INPE), 4 = NRLMSISE-00 (Picone - NRL)
# iF107:  0 = constant F10.7 flux, 1 = variable F10.7 flux
# iSRP:   0 = no SRP perturbation, 1 = SRP, no eclipses, 2 = SRP with conical Earth shadow
# iephem: Ephemerides source. 1 = DE431 ephemerides. 2 = Simpl. Meeus & Brown
# gdeg:   Maximum degree of the gravitational potential.
# gord:   Maximum order of the gravitational potential.
insgrav = 1
isun    = 1
imoon   = 1
idrag   = 4
iF107   = 1
iSRP    = 2
iephem  = 2
gdeg    = 5
gord    = 5

## INTEGRATION
# tolref: Absolute = relative tolerance for the reference propagation
# tol:    Absolute = relative tolerance for the test propagation
# tspan:  Propagation time span (solar days).
# tstep:  Step size (solar days).
# mxstep: Maximum number of integration/output steps.
tol    = 1.000000000000000E-08
tspan  = 10
tstep  = 1
mxstep = 1.0E+06

## EQUATIONS OF MOTION
# eqs: Type of the equations of motion.
# 1 = Cowell, 2 = EDromo(t), 3 = EDromo(c), 4 = EDromo(l),
# 5 = KS t), 6 = KS (l), 7 = Sti-Sche (t), 8 = Sti-Sche (l)
eqs = 4

## OUTPUT SETTINGS
# verb:      		1 = Toggle verbose output, 0 = otherwise
# primaryObjName:   Name of THALASSA output files of primary object (saved to primaryPropPaath)
# secondaryObjName: Name of THALASSA output files of secondary object (saved to secondaryPropPath)
verb = 0
