"""Script to launch brute force conjunction analysis using the THALASSA orbitl Propagator"""

# Import Packages
import os
import shutil
import operator
import json
import multiprocessing
import numpy as np
from joblib import Parallel, delayed

# Import Functions
from genThalassaInputs import genThalassaList
from batchProp import thalassaBatchProp

# Import Settings
import settings as s


def main():
    """main function to be mapped to the command line"""

    # Calculate number of chunks
    chunks = np.ceil(s.tspan/s.chunkSize)
    tspanNorm = s.chunkSize
    tspanFin = 3*(s.tspan/s.chunkSize - int(s.tspan/s.chunkSize))

    # Create output directory tree
    outputDirs = createOutputDir(s.outputDir, chunks)

    # Create thalassa input lists
    genThalassaList(s.primariesInput, '{}'.format(outputDirs['chunk_1']['primaries']), 'primaries')
    genThalassaList(s.secondariesInput , '{}'.format(outputDirs['chunk_1']['secondaries']), 'secondaries')

    chunk = 1
    while chunk <= chunks:

        if chunk == chunks:
            # create batch thalassa propagation configuration inputs
            if tspanFin == 0:
                genBatchPropConfig(outputDirs['chunk_{}'.format(chunk)], tspanNorm)
            else:
                genBatchPropConfig(outputDirs['chunk_{}'.format(chunk)], tspanFin)
        else:
            # create batch thalassa propagation configuration inputs
            genBatchPropConfig(outputDirs['chunk_{}'.format(chunk)], tspanNorm)

        # Propagate objects
        if s.primariesEqualSecondaries is True:
            thalassaBatchProp(s.thalassaDir, '{}/settings.json'.format(outputDirs['chunk_{}'.format(chunk)]['primaries']), '{}/primaries.txt'.format(outputDirs['chunk_{}'.format(chunk)]['primaries']), s.numCores)

            if chunk != chunks:
                # Update object lists
                updateObjectList(outputDirs['chunk_{}'.format(chunk)]['primariesOrbels'], '{}/primaries.txt'.format(outputDirs['chunk_{}'.format(chunk)]['primaries']), '{}/primaries.txt'.format(outputDirs['chunk_{}'.format(chunk + 1)]['primaries']))
            
            # Check for close approaches
            caseDictionary = endogenousFilter(outputDirs['chunk_{}'.format(chunk)]['primariesOrbels'])

            # Create list of input dictionaries for multiprocessing
            inputs = createInputs(caseDictionary, s.conjunctionThreshold)

            # Multiprocess inputs and check for conjuctions
            if s.numCores == 'all':
                numCores = numCores = multiprocessing.cpu_count()
            else:
                numCores = s.numCores
            cases = Parallel(n_jobs = numCores)(delayed(checkForCloseApproach)(i) for i in inputs)

            # Filter out negative cases
            conjunctions = filterConjunctions(cases)
            del cases

            # Print conjunctions
            printConjunctions(conjunctions, outputDirs['chunk_{}'.format(chunk)]['conjunctions'])
            
            # delete orbels
            shutil.rmtree(outputDirs['chunk_{}'.format(chunk)]['primariesOrbels'])

        else:
            thalassaBatchProp(s.thalassaDir, '{}/settings.json'.format(outputDirs['chunk_{}'.format(chunk)]['primaries']), '{}/primaries.txt'.format(outputDirs['chunk_{}'.format(chunk)]['primaries']), s.numCores)
            thalassaBatchProp(s.thalassaDir, '{}/settings.json'.format(outputDirs['chunk_{}'.format(chunk)]['secondaries']), '{}/secondaries.txt'.format(outputDirs['chunk_{}'.format(chunk)]['secondaries']), s.numCores)

            if chunk == chunks:
                return

            # Update object lists
            updateObjectList(outputDirs['chunk_{}'.format(chunk)]['primariesOrbels'], '{}/primaries.txt'.format(outputDirs['chunk_{}'.format(chunk)]['primaries']), '{}/primaries.txt'.format(outputDirs['chunk_{}'.format(chunk + 1)]['primaries']))
            updateObjectList(outputDirs['chunk_{}'.format(chunk)]['secondariesOrbels'], '{}/secondaries.txt'.format(outputDirs['chunk_{}'.format(chunk)]['secondaries']), '{}/secondaries.txt'.format(outputDirs['chunk_{}'.format(chunk + 1)]['secondaries']))
            
            # Check for close approaches
        
        chunk = chunk + 1
        
    return

def filterConjunctions(cases):
    """Function to delete empty cases from conjunctions list"""
    conjunctions = []
    for case in cases:
        if case:
            conjunctions.append(case)
    return conjunctions

def createInputs(caseDictionary, intersectDistance):
    """Function to process specified cases"""

    inputs = []

    for dummy, entry in caseDictionary.items():
        for secondary in entry['secondaries']:
            case = {}
            case['primary'] = entry['primary']
            case['secondary'] = secondary
            case['intersectDistance'] = intersectDistance
            inputs.append(case)
    
    return inputs

def printConjunctions(conjunctions, outputDir):
    """Function to print conjunction results to output directory"""

    for idx, case in enumerate(conjunctions, start = 1):
        path = '{}/{}_{}_{}.json'.format(outputDir, idx, case[0]['primaryDesig'], case[0]['secondaryDesig'])
        with open(path, 'w') as outfile:
            json.dump(case[0], outfile, indent = 2)

    return

def killDsStore(listIn):
    """Function to kill all .DS_Store files in a list"""

    # define indexing
    idx = 0

    # loop through list and delete .DS_Store files
    for dummy in listIn:

        if listIn[idx].endswith(".DS_Store"):

            del listIn[idx]

            # step back indexing
            idx = idx - 1

        # update indexing
        idx = idx + 1

    return listIn

def sortPaths(paths):
    """Function to sort THALASSA output paths using batchProp index"""

    def numFromString(myString):
        """Function to grab a number from a string for sorting"""
        
        val = ""
        for char in myString:
            try:
                dummy = int(char)
                val += char
            except ValueError:
                continue

        return int(val)    
    
    # define dictionary
    pathsDict = {}

    for path in paths:
        desig = numFromString(path)
        pathsDict[desig] = path

    sortedPathsDict = sorted(pathsDict.items(), key=operator.itemgetter(0))

    # define sorted list
    sortedPaths = []

    for entry in sortedPathsDict:
        sortedPaths.append(entry[1])

    return sortedPaths

def grabOrbels(fNames):
    """Function to only add orbels.dat files to the list of files to be read"""

    fNameOrbels = []

    for line in fNames:

        if line[-10:-4] == "orbels":

            fNameOrbels.append(line)

    return fNameOrbels

def grabCarts(fNames):
    """Function to only add cart.dat files to the list of files to be read"""

    fNameOrbels = []

    for line in fNames:

        if line[-8:-4] == "cart":

            fNameOrbels.append(line)

    return fNameOrbels

def combineChars(newObjects, previousObjects):
    """Functions to combine physical elements and states of new and previous orbels"""

    combinedObjs = []

    with open(previousObjects, 'r') as f_obj:
        lines = f_obj.readlines()
    previousObjects = lines[3:]
    for idx, newObject in enumerate(newObjects, start = 0):
        physChars = previousObjects[idx][161:274]
        combinedObj = '{}, {}'.format(newObject, physChars)
        combinedObjs.append(combinedObj)
    
    return combinedObjs

def updateObjectList(inputOrbelsDir, previousObjects, fNameOut):
    """Function to update list Thalassa propagation"""

    # create list of of object paths
    paths = os.listdir(inputOrbelsDir)

    # kill .DSstore.
    objects = killDsStore(paths)

    # grab orbels files
    objects = grabOrbels(objects)

    # sort paths
    objects = sortPaths(objects)

    # populate list of new objects
    newObjects = []
    for obj in objects:
        with open('{}/{}'.format(inputOrbelsDir, obj), 'r') as f_obj:
            lines = f_obj.readlines()
        finalState = lines[-1][1:160]
        newObjects.append(finalState)
    combinedObjs = combineChars(newObjects, previousObjects)

    # print new list of objects
    with open(fNameOut, 'w') as fp:
        # write header
        fp.write('# Updated Keplarian elements formatted for Thalassa\n')
        fp.write('# ===========================================================================================================================================================================================================================================================================================\n')
        fp.write('MJD                      SMA                    ECC                    INC                    RAAN                   AOP                    M                    Mass                   Area_drag              Area_SRP                    CD                     CR\n')
        for obj in combinedObjs:
            fp.write('{}\n'.format(obj))
    
    return

def formatOutputDir(outputDir):
    """Function to format output directory string"""

    if outputDir.endswith('/'):
        return outputDir[:-1]
    
    return outputDir

def createOutputDir(outputDir, chunks):
    """ Function to create output directory structure"""

    # format output directory path
    outputDir = formatOutputDir(outputDir)

    # make directory structure
    makeDir(outputDir)

    # define output directories
    outputDirs = {}
    chunk = 1
    while chunk <= chunks:
        os.makedirs('{}/chunk_{}/input'.format(outputDir, chunk))
        os.makedirs('{}/chunk_{}/propagate'.format(outputDir, chunk))
        os.makedirs('{}/chunk_{}/conjunctions'.format(outputDir, chunk))
        os.makedirs('{}/chunk_{}/input/primaries'.format(outputDir, chunk))
        os.makedirs('{}/chunk_{}/input/secondaries'.format(outputDir, chunk))
        os.makedirs('{}/chunk_{}/propagate/primaries/grid'.format(outputDir, chunk))
        os.makedirs('{}/chunk_{}/propagate/secondaries/grid'.format(outputDir, chunk))
        os.makedirs('{}/chunk_{}/propagate/primaries/orbels'.format(outputDir, chunk))
        os.makedirs('{}/chunk_{}/propagate/secondaries/orbels'.format(outputDir, chunk))
        outputDirs['chunk_{}'.format(chunk)] = {
            'input' : '{}/input'.format(outputDir),
            'primaries' : '{}/chunk_{}/input/primaries'.format(outputDir, chunk),
            'secondaries' : '{}/chunk_{}/input/secondaries'.format(outputDir, chunk),
            'primariesGrid' : '{}/chunk_{}/propagate/primaries/grid'.format(outputDir, chunk),
            'secondariesGrid' : '{}/chunk_{}/propagate/secondaries/grid'.format(outputDir, chunk),
            'primariesOrbels' : '{}/chunk_{}/propagate/primaries/orbels'.format(outputDir, chunk),
            'secondariesOrbels' : '{}/chunk_{}/propagate/secondaries/orbels'.format(outputDir, chunk),
            'conjunctions' : '{}/chunk_{}/conjunctions'.format(outputDir, chunk)
        }
        chunk = chunk + 1

    return outputDirs

def makeDir(directoryPath):
    """Function to delete and recreate directories, "clean", directories"""

    # check if directory exists
    if os.path.isdir(directoryPath) is True:
        # delete directory
        shutil.rmtree(directoryPath)
        # replace directory
        os.makedirs(directoryPath)
    else:
        # create directory
        os.makedirs(directoryPath)	
    return

def genBatchPropConfig(outputDir, tspan):
    """Function to create configuration files for the THALASSA batch propagator"""

    def createBatchConfig(propPath, gridPath, objName, tspan):
        """Function to create THALASSA json settings dictionary"""

        # create settings dictionary
        settings = {}
        # output paths
        settings["outputPath"] = {
            "propPath": propPath
        }
        # physical model
        settings["physicalModel"] = {
            "insgrav": s.insgrav,
            "isun": s.isun,
            "imoon": s.imoon,
            "idrag": s.idrag,
            "iF107": s.iF107,
            "iSRP": s.iSRP,
            "iephem": s.iephem,
            "gdeg": s.gdeg,
            "gord": s.gord
        }
        # integration
        settings["integration"] = {
            "tol": s.tol,
            "tspan": tspan,
            "tstep": s.tstep,
            "mxstep": s.mxstep
        }
        # equations of motion
        settings["eOfM"] = {
            "eqs": s.eqs
        }
        # grid
        settings["grid"] = {
            "gridPath": gridPath,
        }
        # output
        settings["output"] = {
            "verb": s.verb,
            "objName": objName,
        }
        return settings

    def printConfig(settings, path):
        """Function to print settings dictionary to json file"""

        # open file and print
        with open(path, "w") as fp:
            json.dump(settings, fp, indent = 2)

    # create settings dictionaries
    primarySettings   = createBatchConfig(outputDir['primariesOrbels'], outputDir['primariesGrid'], 'primaryEphem', tspan)
    secondarySettings = createBatchConfig(outputDir['secondariesOrbels'], outputDir['secondariesGrid'], 'secondaryEphem', tspan)

    # print settings dictionaries
    printConfig(primarySettings, '{}/settings.json'.format(outputDir['primaries']))
    printConfig(secondarySettings, '{}/settings.json'.format(outputDir['secondaries'])) 

    return 

def checkForCloseApproach(case):
    """Function to check for a close aproach between a given primary and secondary object"""

    # define inputs
    primaryObjPath = case['primary']
    secondaryObjPath = case['secondary']
    intersectDistance = case['intersectDistance']
    #figPath = case['figPath']

    # define object lists
    primaryObj = []
    secondaryObj = []
    time  = []

    # define output
    rSep = []

    # open primary object and read in data
    with open(primaryObjPath, 'r') as f_obj:
        lines = f_obj.readlines()
        lines = lines[3:len(lines)]
    for line in lines:
        state = [x.strip() for x in line.split(',')]
        MJD = state[0]
        x   = state[1]
        y   = state[2]
        z   = state[3]
        vx  = state[4]
        vy  = state[5]
        vz  = state[6]
        state = [x, y, z, vx, vy, vz]
        time.append(float(MJD))
        primaryObj.append(state)

    # open second object and read in data
    with open(secondaryObjPath, 'r') as f_obj:
        lines = f_obj.readlines()
        lines = lines[3:len(lines)]
    for line in lines:
        state = [x.strip() for x in line.split(',')]
        x   = state[1]
        y   = state[2]
        z   = state[3]
        vx  = state[4]
        vy  = state[5]
        vz  = state[6]
        state = [x, y, z, vx, vy, vz]
        secondaryObj.append(state)

    # calculate relative separation vector
    for idx, dummy in enumerate(time):	
        try:
            state_1     = primaryObj[idx]
            r_1_vec = np.array([[float(state_1[0]), float(state_1[1]), float(state_1[2])]])
        except IndexError:
            print("Indexing error in primary object...")
            print("path: {}".format(primaryObjPath))
            print("NaN value likely emerged in THALASSA state vector")
            return
        try:
            state_2     = secondaryObj[idx]
            r_2_vec = np.array([[float(state_2[0]), float(state_2[1]), float(state_2[2])]])
        except IndexError:
            print("Indexing error in secondary object...")
            print("path: {}".format(secondaryObjPath))
            print("NaN value likely emerged in THALASSA state vector") 
            return
        rRel     = r_1_vec - r_2_vec
        rRelMag = (rRel[0,0]**2 + rRel[0,1]**2 + rRel[0,2]**2)**(1/2)

        # add to list
        rSep.append(rRelMag)

    # check for conjunctions
    cases = []
    for step, distance in enumerate(rSep, start = 0):
        if distance < intersectDistance:
            case = {}
            case['relativeSepartion'] = distance
            case['time'] = time[step]
            case['primaryState'] = primaryObj[step]
            case['secondaryState'] = secondaryObj[step]
            case['primaryPath'] = primaryObjPath
            case['secondaryPath'] = secondaryObjPath
            primary = primaryObjPath.split('/')[-1]
            primary = primary.split('cart')[0]
            secondary = secondaryObjPath.split('/')[-1]
            secondary = secondary.split('cart')[0]
            case['primaryDesig'] = primary
            case['secondaryDesig'] = secondary
            cases.append(case)

            return cases
    
    return 

def endogenousFilter(orbelsDir):
    """Function to generate a dictionary of primary and secondary lists in order to prevent double counting when analyzing endogenous collisions"""

    # create list of of object paths
    paths = os.listdir(orbelsDir)

    # kill .DSstore.
    objects = killDsStore(paths)

    # grab orbels files
    objects = grabCarts(objects)

    # sort paths
    objects = sortPaths(objects)

    # add orbels directory to path
    for idx, obj in enumerate(objects, start = 0):
        objects[idx] = '{}/{}'.format(orbelsDir, obj)

    cases2run = {}
    caseDictionary = {}

    for i, dummy in enumerate(objects, start = 0):

        if i == len(objects) - 1:
            break

        secondaries2run = []

        for j, dummy in enumerate(objects, start = i + 1 ):

            if j >= len(objects):
                break
            
            secondaries2run.append(j)
        
        cases2run["{}".format(i)] = secondaries2run
    
    # loop through cases to run and generate lists of primaries and secondaries
    for primaryIdx, secondaryIdxs in cases2run.items():

        # define case dictionary (specifies 1 single primary)
        case = {}

        primary = objects[int(primaryIdx)]

        secondaries = []

        for secondaryIdx in secondaryIdxs:

            secondaries.append(objects[secondaryIdx])

        case["primary"]    = primary
        case["secondaries"] = secondaries

        # add case to dictionary of cases
        caseDictionary["{}".format(primaryIdx)] = case

    return caseDictionary


# map command line arguments to function arguments
# if __name__ == '__main__':

# 	main(*sys.argv[1:])
main()
